import React, { useState, useEffect } from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  Alert,
  Keyboard,
  TouchableWithoutFeedback,
  Text,
  Button,
} from 'react-native';
// import AsyncStorage from '@react-native-community/async-storage';
import Header from './components/Header';
import { v4 as uuid } from 'uuid';
import ListItem from './components/ListItem';
import AddItem from './components/AddItem';
// import {asyncStorage as storage} from './components/utils/asyncStorage';
import { useAsyncStorage } from '@react-native-community/async-storage';

const DismissKeyboard = ({ children }) => (
  <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
    {children}
  </TouchableWithoutFeedback>
);

const App = () => {
  const [isLoading, setIsLoading] = useState(true)
  const [items, setItems] = useState([
    // {id: uuid(), text: 'Milk', quantity: 1},
    // {id: uuid(), text: 'Milk', quantity: 2.6},
    // {id: uuid(), text: 'Eggs', quantity: 1},
    // {id: uuid(), text: 'Bread', quantity: 1},
    // {id: uuid(), text: 'Juice', quantity: 1},
    // {id: uuid(), text: 'Coffee', quantity: 1},
  ]);
  const { getItem, setItem, removeItem } = useAsyncStorage('@shopping_list');

  const [shopItem, setShopItem] = useState({});
  const [type, setType] = useState('Add');

  // const [items, setItems] = useState([]);

  const [selected, setSelected] = React.useState(new Map());

  const readItemFromStorage = async () => {
    let item = [];
    try {
      item = await getItem();
      item = JSON.parse(item);
      // console.log('read items', item);
      if (items.length === 0 && item) {
        setItems(item, console.log('setted read items '));
      } else if (isLoading) {
        setIsLoading(false)
        setItems([])
      } else {
        // setItems(item)
      }
    } catch (error) {
      console.error('read error', error);
      item = [];
      setItems(item);
    }
    // setItems(item);
  };

  const writeItemToStorage = async newValue => {
    newValue = JSON.stringify([newValue, ...items]);
    // console.log('the new value to write', newValue);
    try {
      await setItem(newValue, console.log('setted item to storage', getItem()));
    } catch (error) {
      console.error('error writing', error);
    }
    // setItems(newValue);
  };

  const updateItemToStorage = async newValue => {
    // let prevItems = items
    // const cnt = prevItems.length;
    // for (let i = 0; i < cnt; i++) {
    //   if (prevItems[i].id === newItem.id) {
    //     console.info(prevItems[i], newItem);
    //     prevItems[i] = newItem;
    //     break;
    //   }
    // }


    try {
      // console.log('object', newValue, items)
      let it = items.map(item => {
        if (item.id === newValue.id) {
          return newValue
        } else {
          return item
        }
      })
      console.info('it', it, JSON.stringify(it))
      await setItem(
        JSON.stringify(it),
        console.log('updated item to storage', getItem()),
        // getItem().map((item, index) => {
        //   console.log('item+index', item, index)
        // }),
      );
    } catch (error) {
      console.error('error updating', error);
    }


    // setItems(newValue);
  };

  const removeItemsFromStorage = async () => {
    await removeItem();
    setItems([]);
  };

  const deleteItemFromStorage = async id => {
    try {
      await setItem(
        JSON.stringify(items.filter(item => item.id !== id)),
        console.log('setted item to storage', getItem()),
      );
      console.info(items);
    } catch (e) {
      console.error(e);
    }
  };

  useEffect(() => {
    readItemFromStorage();
    // removeItemsFromStorage();
  }, []);

  const deleteItem = id => {
    Alert.alert(
      'Delete item',
      'Confirm deletion?',
      [
        {
          text: 'Cancel',
          onPress: () => { },
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () =>
            setItems(
              prevItems => prevItems.filter(item => item.id !== id),
              deleteItemFromStorage(id),
            ),
        },
      ],
      { cancelable: false },
    );
  };

  const addItem = async (text, textQuantity) => {
    // console.log(text, textQuantity, textQuantity === '');
    if (!text) {
      Alert.alert('Error', 'Please enter an item', [{ text: 'OK' }]);
    } else if (!textQuantity) {
      Alert.alert('Error', 'Please enter a quantity', [{ text: 'OK' }]);
    } else {
      const newItem = { id: uuid(), text, quantity: textQuantity };
      // try {
      //   const jsonValue = JSON.stringify(newItem);
      //   await storage.setObjectValue(jsonValue);
      // } catch (e) {
      //   // saving error
      // }
      setItems(
        prevItems => [newItem, ...prevItems],
        writeItemToStorage(newItem),
      );
      Keyboard.dismiss()
    }
  };

  const restoreAdd = () => {
    setType('Add');
    setShopItem({});
  }

  const editItem = id => {
    // console.log(id);
    setType('Edit');
    const it = items.filter(item => item.id === id)[0];
    console.info('editItem', it);
    setShopItem(it, console.log('shopitem', shopItem));
  };

  const updateItem = (id, text, quantity, index) => {
    // console.log(id, text, quantity);
    setType('Add');
    setShopItem({});
    if (!text) {
      Alert.alert('Error', 'Please enter an item', [{ text: 'OK' }]);
    } else if (!quantity) {
      Alert.alert('Error', 'Please enter a quantity', [{ text: 'OK' }]);
    }
    const newItem = { id, text, quantity };
    console.info(newItem)
    setItems(prevItems => {
      const cnt = prevItems.length;
      for (let i = 0; i < cnt; i++) {
        if (prevItems[i].id === newItem.id) {
          console.info(prevItems[i], newItem);
          prevItems[i] = newItem;
          break;
        }
      }
    }, updateItemToStorage(newItem))
    return;
    if (!text) {
      Alert.alert('Error', 'Please enter an item', [{ text: 'OK' }]);
    } else if (!quantity) {
      Alert.alert('Error', 'Please enter a quantity', [{ text: 'OK' }]);
    } else {
      const newItem = { id, text, quantity };
      // try {
      //   const jsonValue = JSON.stringify(newItem);
      //   await storage.setObjectValue(jsonValue);
      // } catch (e) {
      //   // saving error
      // }
      setItems(prevItems => {
        // setTodos({ ...todos,[todo.id]: todo }),

        // prevItems.map(item => {
        //   if (item.id === newItem.id) {
        //     item = newItem;
        //   }
        // })
        // prevItems.forEach((element, i) => {
        //   if (element.id === newItem.id) {
        //     prevItems[i] = newItem;
        //   }
        // });
        const cnt = prevItems.length;
        for (let i = 0; i < cnt; i++) {
          if (prevItems[i].id === newItem.id) {
            console.info(prevItems[i], newItem);
            prevItems[i] = newItem;
            break;
          }
        }
        // for (const key of prevItems) {
        //   if (key.id === newItem.id) {
        //     prevItems[key] = newItem;
        //   }
        // }
      });
    }
  };

  const check = (checkedVal, id) => {
    // console.log(checkedVal, id);
  };

  const onSelect = React.useCallback(
    id => {
      // console.log(id);
      const newSelected = new Map(selected);
      newSelected.set(id, !selected.get(id));

      setSelected(newSelected);
    },
    [selected],
  );

  const ListViewItemSeparator = () => {
    return (
      //List Item separator View
      <View style={{ height: 0.5, width: '100%', backgroundColor: '#606070' }} />
    );
  };

  const renderHeader = () => {
    //View to set in Header
    return items.length > 0 && (
      <View style={styles.header_footer_style}>
        <Text style={styles.textStyleHeader}> List of items </Text>
      </View>
    );
  };

  const renderFooter = () => {
    //View to set in Footer
    return (
      <View style={styles.header_footer_style}>
        <Text style={styles.textStyleFooter}>
          {' '}
          Copyright Simon Cateau 2020{' '}
        </Text>
      </View>
    );
  };

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View style={styles.container}>
      <View style={styles.contentContainer}>
        <Header />
        <AddItem
          addItem={addItem}
          addType={type}
          item={shopItem}
          updateItem={updateItem}
        />
        {items && (
          <FlatList
            data={items}
            //dataSource to add data in the list
            ListHeaderComponent={renderHeader}
            //Header to show above listview
            // ListFooterComponent={renderFooter}
            //Footer to show below listview
            ItemSeparatorComponent={ListViewItemSeparator}
            // onPress={({item}) => onSelect(item.id)}
            // style={[
            //   styles.item,
            //   {backgroundColor: selected ? '#6e3b6e' : '#f9c2ff'},
            // ]}
            renderItem={({ item }) => (
              <ListItem
                item={item}
                deleteItem={deleteItem}
                check={check}
                editItem={editItem}
              />
            )}
          />
        )}


        </View>
{/* <Button title="Edit" onPress={editItem} /> */}
        <View>
         {items && items.length === 0 && (
          <Button style={styles.buttons} title="Read Stored Data" onPress={() => { readItemFromStorage(); restoreAdd(); }} />)}
        {items && items.length > 0 && (
          <Button style={styles.buttons} title="Clear Stored Data" onPress={() => { removeItemsFromStorage(); restoreAdd() }} />)}
          </View>
        {renderFooter()}

      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // paddingTop: 60,
  },
  contentContainer: {
    flex: 1,
  },
  header_footer_style: {
    width: '100%',
    height: 45,
    backgroundColor: '#606070',
  },
  textStyle: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 18,
    fontSize: 12,
    padding: 7,
  },
  textStyleHeader: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 18,
    padding: 7,
  },
  textStyleFooter: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 12,
    padding: 7,
  },
  buttons: {
    borderColor: "silver",
    borderWidth: 5,
    backgroundColor: "#000000"
  }
});

export default App;
