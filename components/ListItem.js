import React, {useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Alert} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import AppModal from './AppModal';
import CheckBox from '@react-native-community/checkbox';

const ListItem = ({item, deleteItem, check, editItem}) => {
  const [toggleCheckBox, setToggleCheckBox] = useState(false);

  const handlerLongClick = () => {
    //handler for Long Click
    Alert.alert(' Button Long Pressed');
  };

  return (
    <TouchableOpacity
      style={styles.listItem}
      onLongPress={handlerLongClick}
      //Here is the trick
      activeOpacity={0.6}>
      <View style={styles.listItemView}>
        {/* <CheckBox
          disabled={false}
          value={toggleCheckBox}
          onValueChange={newValue => {
            setToggleCheckBox(newValue);
            check(newValue, item.id);
          }}
        /> */}
        <View style={styles.itemsView}>
          <Text style={styles.listItemText}>{item.text}</Text>
          <Text style={styles.listItemText}> {item.quantity} </Text>
        </View>
        <View style={styles.actionsView}>
          <Icon
            name="edit"
            size={20}
            // color=
            onPress={() => editItem(item.id)}
          />
          <Icon
            name="remove"
            size={20}
            color="firebrick"
            onPress={() => deleteItem(item.id)}
          />
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  listItem: {
    padding: 15,
    backgroundColor: '#f8f8f8',
    borderBottomWidth: 1,
    borderColor: '#eee',
  },
  listItemView: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  itemsView: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: '80%',
  },
  actionsView: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    width: '20%',
  },
  listItemText: {
    fontSize: 18,
  },
});

export default ListItem;
