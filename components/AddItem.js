import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

const AddItem = ({ addItem, updateItem, item, addType }) => {
  const [text, setText] = useState(item.text);
  const [textQuantity, setTextQuantity] = useState(item.quantity);
  let textInput = React.createRef();
  let textInputQuantity = React.createRef();
  let textId = React.createRef();

  const onChangeName = textValue => setText(textValue);
  const onChangeQuantity = textValue => setTextQuantity(textValue);

  useEffect(() => {
    setText(item.text);
    setTextQuantity(item.quantity);
    // console.info('item effect', item, text, textQuantity);
  }, [item]);

  const handleAddItem = () => {
    if (addType === 'Add') {
      addItem(text, textQuantity);
    } else {
      console.info(item, text, textQuantity, '>>>');
      updateItem(item.id, text, textQuantity, item.index);
    }
    setText('');
    setTextQuantity('');
  };

  return (
    <View>
      <TextInput
        placeholder={addType + ' Item...'}
        style={styles.input}
        ref={textInput}
        onChangeText={onChangeName}
        value={text}
        clearButtonMode="always"
      />
      <TextInput
        placeholder="Quantity..."
        style={styles.input}
        ref={textInputQuantity}
        onChangeText={onChangeQuantity}
        value={textQuantity}
        clearButtonMode="always"
        underlineColorAndroid="transparent"
        keyboardType="decimal-pad"
        autoCapitalize="words"
      />

      <TouchableOpacity style={styles.btn} onPress={() => handleAddItem()}>
        <Text
          style={styles.btnText}
          ref={(input, inputQuantity) => {
            textInput = input;
            inputQuantity = inputQuantity;
          }}>
          {' '}
          <Icon name={addType === 'Add' ? 'plus' : 'edit'} size={20} />
          {' ' }{addType} Item
        </Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    height: 60,
    padding: 8,
    fontSize: 16,
  },
  btn: {
    backgroundColor: '#c2bad8',
    padding: 9,
    margin: 5,
  },
  btnText: {
    color: 'darkslateblue',
    fontSize: 20,
    textAlign: 'center',
  },
});

export default AddItem;
