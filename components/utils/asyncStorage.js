import storage from '@react-native-community/async-storage';

const app_key = 'shopping_list';

export const asyncStorage = () => {
  const getData = async () => {
    console.log('in getData');
    try {
      console.log('try reading');
      const jsonValue = await storage.getItem(app_key);
      console.log('json data', jsonValue);
      return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch (e) {
      // error reading value
      console.error('error reading', e);
      return [];
    }
  };

  const setObjectValue = async value => {
    try {
      const jsonValue = JSON.stringify(value);
      await storage.setItem(app_key, jsonValue);
    } catch (e) {
      // save error
      console.error('error saving', e);
    }

    console.log('Done.');
  };

  const removeValue = async () => {
    try {
      await storage.removeItem(app_key);
    } catch (e) {
      // remove error
      console.error('remove error', e);
    }

    console.log('Done.');
  };
};
